Flask==1.1.2
requests==2.25.1
gunicorn==20.1.0
marshmallow==3.11.1
Flask-SQLAlchemy==2.5.1
Flask-Migrate==2.2.1
Flask-Script==2.0.6
flake8==3.9.1
python-dotenv==0.17.0
psycopg2-binary==2.8.6
pytest==3.0.4
pytest-cov==2.5.1
-e .
