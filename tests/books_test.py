import json
import unittest
from unittest import mock
from unittest.mock import Mock
import os

from flask_migrate import Migrate

from api.app import application
from api.models import Book


class ApiTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.app = application.test_client()

    @mock.patch('api.models.Book.query')
    @mock.patch('api.db')
    def test_given_request_to_book_should_return_200(self, db_mock, book_model_mock):
        book = Book()
        book.id = 1
        book.title = 'Test Book'
        book.isbn = '123456'
        book.description = 'lorem ipsum dolor'
        book.author = 'Test Author'
        book_model_mock.all.return_value = [book]

        response = self.app.get('/v1/books')
        self.assertEqual(response.status_code, 200)
