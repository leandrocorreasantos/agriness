DOCKER_CMD=docker exec -it agriness-api
DOCKER_TEST_CMD=docker exec -it -e APP_SETTINGS=api.config.TestingConfig -e DATABASE_URL="sqlite:///:memory:" -e CACHE_TYPE=simple agriness-api
_upgrade-pip:
	${DOCKER_CMD} pip install --upgrade pip

_config-env:
	[ -f .env ] || cp .env.sample .env

init: _config-env
	docker-compose up -d

setup: _upgrade-pip
	${DOCKER_CMD} pip install -r requirements.txt

flake8:
	${DOCKER_CMD} flake8 --exclude=venv,migrations,api/manager.py,tests .

test:
	${DOCKER_TEST_CMD} pytest tests
ssh:
	${DOCKER_CMD} bash

start:
	${DOCKER_CMD} python api/app.py

migrate-init:
	${DOCKER_CMD} python api/manager.py db init

migrate:
	${DOCKER_CMD} python api/manager.py db migrate

migrate-apply:
	${DOCKER_CMD} python api/manager.py db upgrade

migrate-new-version:
	${DOCKER_CMD} python api/manager.py db revision

migrate-rollback:
	${DOCKER_CMD} python api/manager.py db downgrade -1

seed:
	${DOCKER_CMD} python api/manager.py seed
