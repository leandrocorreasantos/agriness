# agriness

API Rest em Python

## Instalação

Requisitos mínimos:

- Docker

Dentro do diretório da aplicação, executar os seguintes comandos:

### Inicializando aplicação

```bash
make init
```

### Instalando dependências

```bash
make setup
```

### criando as tabelas do banco de dados

```bash
make migrate-apply
```

### Inicializando a API com os primeiros dados fake para testes

```bash
make seed
```

### Inicializando localmente

```bash
make start
```

Após seguir os passos acima, a API estará disponível no endereço http://localhost:5001, podendo ser acessada através dos endereços abaixo:


Protocolo | endereço
---|---
GET | http://localhost:5001/v1/books
GET | http://localhost:5001/v1/clients
GET | http://localhost:5001/v1/client/1/books
POST | http://localhost:5001/v1/books/1/reserve?client_id=1
