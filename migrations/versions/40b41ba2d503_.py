"""initial structure
Revision ID: 40b41ba2d503
Revises:
Create Date: 2021-04-19 08:39:51.473214

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '40b41ba2d503'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('books',
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.Column('title', sa.String(length=300), nullable=False),
    sa.Column('isbn', sa.String(length=60), nullable=False),
    sa.Column('description', sa.Text(), nullable=True),
    sa.Column('author', sa.String(length=300), nullable=True),
    sa.Column('reserved', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('states',
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.Column('name', sa.String(length=100), nullable=False),
    sa.Column('fs', sa.String(length=2), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('cities',
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.Column('name', sa.String(length=100), nullable=False),
    sa.Column('state_id', sa.BigInteger(), nullable=True),
    sa.ForeignKeyConstraint(['state_id'], ['states.id'], onupdate='CASCADE', ondelete='RESTRICT'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('clients',
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.Column('name', sa.String(length=255), nullable=False),
    sa.Column('address', sa.String(length=255), nullable=True),
    sa.Column('address_number', sa.String(length=10), nullable=True),
    sa.Column('neighborhood', sa.String(length=255), nullable=True),
    sa.Column('city_id', sa.BigInteger(), nullable=True),
    sa.Column('email', sa.String(length=255), nullable=True),
    sa.Column('telephone', sa.String(length=30), nullable=True),
    sa.ForeignKeyConstraint(['city_id'], ['cities.id'], onupdate='CASCADE', ondelete='RESTRICT'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('reserves',
    sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
    sa.Column('client_id', sa.BigInteger(), nullable=True),
    sa.Column('book_id', sa.BigInteger(), nullable=True),
    sa.Column('reserve_date_start', sa.DateTime(), nullable=True),
    sa.Column('reserve_date_end', sa.DateTime(), nullable=False),
    sa.Column('devolution_date', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['book_id'], ['books.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['client_id'], ['clients.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('reserves')
    op.drop_table('clients')
    op.drop_table('cities')
    op.drop_table('states')
    op.drop_table('books')
    # ### end Alembic commands ###
