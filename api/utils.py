def verify_fee(days=0):
    fees = {'fee': 0, 'daily_fee': 0}
    if 0 < days <= 3:
        fees['fee'] = 3
        fees['daily_fee'] = 0.2
    elif 3 < days <= 5:
        fees['fee'] = 5
        fees['daily_fee'] = 0.4
    elif days > 5:
        fees['fee'] = 7
        fees['daily_fee'] = 0.6

    return fees
