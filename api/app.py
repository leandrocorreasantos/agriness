import os
from api import app as application
from api.v1.resources import BookView as v1_Book
from api.v1.resources import ClientView as v1_Client
from api.v1.resources import ReserveView as v1_Reserve


application.add_url_rule(
    '/v1/books',
    view_func=v1_Book.as_view('books'),
    methods=['GET']
)

application.add_url_rule(
    '/v1/clients',
    view_func=v1_Client.as_view('clients'),
    methods=['GET']
)

application.add_url_rule(
    '/v1/client/<client_id>/books',
    view_func=v1_Reserve.as_view('client_books'),
    methods=['GET']
)

application.add_url_rule(
    '/v1/books/<book_id>/reserve',
    view_func=v1_Reserve.as_view('reserve'),
    methods=['POST']
)


if __name__ == '__main__':
    application.run(
        debug=application.debug,
        host=os.environ.get('HOST', '0.0.0.0'),
        port=int(os.environ.get('PORT', 5001))
    )
