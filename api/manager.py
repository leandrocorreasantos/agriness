import sys
import errno
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from api import app, db, log

from api.models import (
    State, City, Book, Client, Reserve
)

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

@manager.command
def seed():
    # initialize data from api
    states = [
        {'id': 1, 'name': 'Santa Catarina', 'fs': 'SC'},
        {'id': 2, 'name': 'Mato Grosso', 'fs': 'MT'}
    ]
    for state in states:
        new_state = State(**state)
        try:
            db.session.add(new_state)
            db.session.commit()
        except Exception as e:
            log.error('Error while seed state data: {}'.format(e))

    cities = [
        {'id': 1, 'name': 'Florianópolis', 'state_id': 1},
        {'id': 2, 'name': 'Sinop', 'state_id': 2}
    ]
    for city in cities:
        new_city = City(**city)
        try:
            db.session.add(new_city)
            db.session.commit()
        except Exception as e:
            log.error('Error while seed city data: {}'.format(e))

    books = [
        {
            'id': 1,
            'title': 'Demo Book 1',
            'isbn': '25-354687-01',
            'description': 'Demonstration book',
            'author': 'Demonstration Author',
            'reserved': True
        },
        {
            'id': 2,
            'title': 'Demo Book 2',
            'isbn': '25-354687-02',
            'description': 'Second Demonstration book',
            'author': 'Demonstration Author',
            'reserved': False
        }
    ]
    for book in books:
        new_book = Book(**book)
        try:
            db.session.add(new_book)
            db.session.commit()
        except Exception as e:
            log.error('Error while seed book data: {}'.format(e))

    clients = [
        {
            'id': 1,
            'name': 'John Doe',
            'address': 'Street 1',
            'address_number': '123',
            'neighborhood': 'center',
            'city_id': 1,
            'email': 'johndoe@localhost.com',
            'telephone': '(123)96633-8855'
        },
        {
            'id': 2,
            'name': 'Jane Doe',
            'address': 'Street 2',
            'address_number': '789',
            'neighborhood': 'Pioneers',
            'city_id': 2,
            'email': 'janedoe@localhost.com',
            'telephone': '(456)94256-9124'
        }
    ]
    for client in clients:
        new_client = Client(**client)
        try:
            db.session.add(new_client)
            db.session.commit()
        except Exception as e:
            log.error('Error while seed client data: {}'.format(e))

    reserve = {
        'id': 1,
        'book_id': 1,
        'client_id': 1,
        'reserve_date_start': '2021-04-05',
        'reserve_date_end': '2021-04-08',
        'devolution_date': '2021-04-09'
    }
    new_reserve = Reserve(**reserve)
    try:
        db.session.add(new_reserve)
        db.session.commit()
    except Exception as e:
        log.error('Error while seed reserve data: {}'.format(e))

if __name__ == '__main__':
    if not app.debug:
        print('App is in production mode. Migration skipped')
        sys.exit(errno.EACCES)
    manager.run()
