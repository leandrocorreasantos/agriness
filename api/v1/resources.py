from api import log, db
from datetime import datetime, timedelta
from flask import request
from flask.views import MethodView

from api.v1.schema import (
    BookSchema,
    ClientSchema,
    ReserveSchema,
    BookNotFoundSchema,
    ClientNotFoundSchema,
    ReserveNotFoundSchema,
    ReserveDatabaseErrorSchema
)
from api.models import (
    Book, Client, Reserve
)
from api.utils import verify_fee


class BookView(MethodView):
    def get(self):
        books = []
        try:
            books = Book.query.all()
        except Exception as e:
            log.error('Book not found {}'.format(e))

        if not len(books):
            return BookNotFoundSchema.build()

        return BookSchema(many=True).build_response(books)


class ClientView(MethodView):
    def get(self):
        clients = Client.query.all()
        if not len(clients):
            return ClientNotFoundSchema.build()

        return ClientSchema(many=True).build_response(clients)


class ReserveView(MethodView):
    def get(self, client_id):
        reserves = []
        result = []
        try:
            reserves = Reserve.query.filter(
                Reserve.client_id == client_id
            ).all()
        except Exception as e:
            log.error('Reserves not found: {}'.format(e))
            return ReserveNotFoundSchema.build()

        for reserve in reserves:
            end_date = datetime.strptime(
                str(reserve.reserve_date_end), '%Y-%m-%d %H:%M:%S'
            )
            devolution_date = datetime.strptime(
                str(reserve.devolution_date), '%Y-%m-%d %H:%M:%S'
            )
            delay = abs((devolution_date - end_date).days)
            fee_reserve = verify_fee(delay)

            reserve.delay = delay
            reserve.fee = fee_reserve['fee']
            reserve.daily_fee = fee_reserve['daily_fee']
            result.append(reserve)

        return ReserveSchema(many=True).build_response(result)

    def post(self, book_id):
        book = Book.query.get(book_id)
        if not book:
            log.error('book not found')
            return BookNotFoundSchema.build()

        client_id = request.args.get('client_id', None)
        if not client_id:
            return ClientNotFoundSchema.build()

        client = Client.query.get(client_id)
        if not client:
            return ClientNotFoundSchema.build()

        reserve = Reserve()
        reserve.client_id = client_id
        reserve.book_id = book_id
        reserve.reserve_date_start = datetime.now()
        reserve.reserve_date_end = datetime.now() + timedelta(days=3)

        try:
            db.session.add(reserve)
            db.session.commit()
        except Exception as e:
            log.error('reserve not saved: {}'.format(e))
            return ReserveDatabaseErrorSchema.build()

        return ReserveSchema.build_response(reserve)
