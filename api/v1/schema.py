from http.client import (
    NOT_FOUND, OK, CREATED, INTERNAL_SERVER_ERROR
)
from flask import jsonify
from marshmallow import Schema, fields
from api.models import (
    Book, Client, State, City, Reserve
)


class BaseSchema(Schema):
    def build_response(self, params):
        return jsonify(self.dump(params)), OK.value

    @classmethod
    def build_created_response(cls):
        return jsonify({}), CREATED.value

    @classmethod
    def build_updated_or_deleted_response(cls):
        return jsonify({}), OK.value


class StateSchema(BaseSchema):
    class Meta:
        __model__ = State

    id = fields.Integer()
    name = fields.String(required=True)
    fs = fields.String(required=True)


class CitySchema(BaseSchema):
    class Meta:
        __model__ = City

    id = fields.Integer()
    name = fields.String(required=True)
    state = fields.Nested('StateSchema')


class BookSchema(BaseSchema):
    class Meta:
        __model__ = Book

    id = fields.Integer()
    title = fields.String(required=True)
    isbn = fields.String(required=True)
    description = fields.String()
    author = fields.String()
    reserved = fields.Boolean()


class ClientSchema(BaseSchema):
    class Meta:
        __model__ = Client

    id = fields.Integer()
    name = fields.String(required=True)
    address = fields.String()
    address_number = fields.String()
    neighborhood = fields.String()
    city_id = fields.Integer(load_only=True)
    city = fields.Nested('CitySchema', dump_only=True)
    email = fields.String()
    telephone = fields.String()


class ReserveSchema(BaseSchema):
    class Meta:
        __model__ = Reserve

    id = fields.Integer()
    client_id = fields.Integer(load_only=True)
    client = fields.Nested('ClientSchema', dump_only=True)
    book_id = fields.Integer(load_only=True)
    book = fields.Nested('BookSchema', dump_only=True)
    reserve_date_start = fields.DateTime()
    reserve_date_end = fields.DateTime(required=True)
    devolution_date = fields.DateTime()
    delay = fields.Integer(dump_only=True)
    fee = fields.Float(dump_only=True)
    daily_fee = fields.Float(dump_only=True)


''' not found schemas '''


class BaseNotFoundSchema(Schema):
    code = fields.Integer(default=NOT_FOUND.value)

    @classmethod
    def build(cls):
        return jsonify(cls().dump({})), NOT_FOUND.value


class BookNotFoundSchema(BaseNotFoundSchema):
    message = fields.String(default="Book not found")
    description = fields.String(
        default="Books could not be found with this parameters"
    )


class ClientNotFoundSchema(BaseNotFoundSchema):
    message = fields.String(default="Client not found")
    description = fields.String(
        default="Clients could not be found with this parameters"
    )


class ReserveNotFoundSchema(BaseNotFoundSchema):
    message = fields.String(default="Reserve not found")
    description = fields.String(
        default="Reserves could not be found with this parameters"
    )


''' database erros schemas '''


class BaseDatabaseErrorSchema(Schema):
    code = fields.Integer(default=INTERNAL_SERVER_ERROR.value)
    description = fields.String(default="Database failed during save data")

    @classmethod
    def build(cls):
        return jsonify(cls().dump({})), INTERNAL_SERVER_ERROR.value


class ReserveDatabaseErrorSchema(BaseDatabaseErrorSchema):
    message = fields.String(default="Reserve database error")
