from api import db
from datetime import datetime


class State(db.Model):
    __tablename__ = 'states'

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    fs = db.Column(db.String(2), nullable=False)


class City(db.Model):
    __tablename__ = 'cities'

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    state_id = db.Column(db.BigInteger, db.ForeignKey(
        State.id,
        onupdate='CASCADE',
        ondelete='RESTRICT'
    ))
    state = db.relationship('State', backref='city')


class Book(db.Model):
    __tablename__ = 'books'

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    title = db.Column(db.String(300), nullable=False)
    isbn = db.Column(db.String(60), nullable=False)
    description = db.Column(db.Text())
    author = db.Column(db.String(300))
    reserved = db.Column(db.Boolean(), default=False, nullable=True)


class Client(db.Model):
    __tablename__ = 'clients'

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    address = db.Column(db.String(255))
    address_number = db.Column(db.String(10))
    neighborhood = db.Column(db.String(255))
    city_id = db.Column(
        db.BigInteger(),
        db.ForeignKey(
            City.id,
            onupdate='CASCADE',
            ondelete='RESTRICT'
        )
    )
    city = db.relationship(
        'City',
        backref='clients',
        lazy='joined'
    )
    email = db.Column(db.String(255))
    telephone = db.Column(db.String(30))


class Reserve(db.Model):
    __tablename__ = 'reserves'

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    client_id = db.Column(
        db.BigInteger,
        db.ForeignKey(
            Client.id,
            onupdate='CASCADE',
            ondelete='CASCADE'
        )
    )
    client = db.relationship(
        'Client',
        backref='reserves',
        lazy='joined'
    )
    book_id = db.Column(
        db.BigInteger,
        db.ForeignKey(
            Book.id,
            onupdate='CASCADE',
            ondelete='CASCADE'
        )
    )
    book = db.relationship(
        'Book',
        backref='reserves',
        lazy='joined'
    )
    reserve_date_start = db.Column(db.DateTime, default=datetime.now)
    reserve_date_end = db.Column(db.DateTime, nullable=False)
    devolution_date = db.Column(db.DateTime)
