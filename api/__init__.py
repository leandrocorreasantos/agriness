import logging
import logging.config
import os
import sys

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

dotenv_path = os.path.join(os.getcwd(), '.env')
if os.path.isfile(dotenv_path):
    from dotenv import load_dotenv
    load_dotenv(dotenv_path)

app.config.from_object(os.environ.get('APP_SETTINGS'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = os.environ.get(
    'SQLALCHEMY_TRACK_MODIFICATIONS', False
)

db = SQLAlchemy(app)

handler = logging.StreamHandler(sys.stdout)
formater = logging.Formatter(
    '{"timestamp": "%(asctime)s",'
    '"level": "%(levelname)s",'
    '"message": "%(message)s"}'
)
handler.setFormatter(formater)

log = logging.getLogger(os.environ.get('LOGGER_NAME', __name__))
log.setLevel(int(os.environ.get('LOG_LEVEL', logging.INFO)))
log.addHandler(handler)
